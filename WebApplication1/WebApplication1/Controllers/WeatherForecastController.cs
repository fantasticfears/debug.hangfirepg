﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NodaTime;

namespace WebApplication1.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class WeatherForecastController : ControllerBase
  {
    private static readonly string[] Summaries = new[]
    {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

    private readonly ILogger<WeatherForecastController> _logger;
    private readonly MyContext _context;

    public WeatherForecastController(ILogger<WeatherForecastController> logger, MyContext context)
    {
      _logger = logger;
      _context = context;
    }

    [HttpGet]
    public IEnumerable<WeatherForecast> Get()
    {
      var rng = new Random();
      //var blog = new Blog { CreatedAt = SystemClock.Instance.GetCurrentInstant(), Url = "https://github.com/frankhommers/Hangfire.PostgreSql" };
      //_context.Add(blog);
      //_context.SaveChanges();
      // var a = blog.BlogId;
      // BackgroundJob.Enqueue(() => Console.WriteLine(2));

      BackgroundJob.Enqueue(() => Console.WriteLine(1));
      return Enumerable.Range(1, 5).Select(index => new WeatherForecast
      {
        Date = DateTime.Now.AddDays(index),
        TemperatureC = rng.Next(-20, 55),
        Summary = Summaries[rng.Next(Summaries.Length)]
      })
      .ToArray();
    }
  }
}
